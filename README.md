# Robotics_Case_Studies

These are examples of my research / analysis writings on a number of robotics-related case study questions, which are similar to those asked on the Robotics PhD Qualifier Exams (https://www.cc.gatech.edu/academics/degree-programs/phd/robotics).

The topics include an analysis of experiments involving mobile healthcare robots, emergency evacuation robot guide prototype design, and research on new safety features of self-driving vehicles for increased driver engagement.
